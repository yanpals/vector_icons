// Carrer avatars

// Careers
var careers = {
edu_avata:'<div ico="edu_avata"><div ico-obj1="edu_avata"></div><div ico-obj2="edu_avata"></div></div>',
	game_avata:'<div ico="game_avata"><div ico-obj1="game_avata"></div><div ico-obj2="game_avata"></div><div ico-obj3="game_avata"></div></div>',
	medicals_avata:'<div ico="medical_avata"><div ico-obj="medical_avata"></div></div>',
	
//MANAGEMENT CATEGORY BEGINS HERE
accounting_avtr:'<div ico="acntin_avtr"><div ico-obj1="acntin_avtr"></div><div ico-obj2="acntin_avtr"></div><div ico-obj3="acntin_avtr"></div></div>',

	chrt_lossadj_avtr:'<div ico="chrt_lossadj_avtr"><div ico-obj1="chrt_lossadj_avtr"></div><div ico-obj2="chrt_lossadj_avtr"></div></div>',
	
	economist_avtr:'<div ico="econs_avtr"><div ico-obj1="econs_avtr"></div><div ico-obj2="econs_avtr"></div><div ico-obj3="econs_avtr"></div></div>',
	
	mgt_consulting_avtr:'<div ico="mgt_consltn_avtr"><div ico-obj1="mgt_consltn_avtr"></div><div ico-obj2="mgt_consltn_avtr"></div><div ico-obj3="mgt_consltn_avtr"></div><div ico-obj4="mgt_consltn_avtr"></div><div ico-obj5="mgt_consltn_avtr"></div></div>',
	
	secretary_avtr: '<div ico="sec_avtr"><div ico-obj1="sec_avtr"></div><div ico-obj2="sec_avtr"></div><div ico-obj3="sec_avtr"></div><div ico-obj4="sec_avtr"></div></div>',
//MANAGEMENT CATEGORY ENDS HERE

//GAMING CATEGORY BEGINS HERE
comptr_game_avtr:'<div ico="comptr_game_avtr"><div ico-obj1="comptr_game_avtr"></div><div ico-obj2="comptr_game_avtr"></div><div ico-obj3="comptr_game_avtr"></div><div ico-obj4="comptr_game_avtr"></div><div ico-obj5="comptr_game_avtr"></div></div>',

gamedev_avtr:'<div ico="gamedev_avtr"><div ico-obj1="gamedev_avtr"></div><div ico-obj2="gamedev_avtr"></div><div ico-obj3="gamedev_avtr"></div><div ico-obj4="gamedev_avtr"></div><div ico-obj5="gamedev_avtr"></div><div ico-obj6="gamedev_avtr"></div></div>',

gamedesigner_avtr:'<div ico="gamedesigner_avtr"><div ico-obj1="gamedesigner_avtr"></div><div ico-obj2="gamedesigner_avtr"></div><div ico-obj3="gamedesigner_avtr"></div></div>',
//GAMING CATEGORY ENDS HERE

//HOSPITALITY CATEGORY BEGINS HERE
hospitalmgt_avtr:'<div ico="hostplmgt_avtr"><div ico-obj1="hostplmgt_avtr"></div><div ico-obj2="hostplmgt_avtr"></div></div>',

accomodationmgt_avtr:'<div ico="acomdatnmgt_avtr"><div ico-obj1="acomdatnmgt_avtr"></div><div ico-obj2="acomdatnmgt_avtr"></div></div>',

cateringmngr_avtr:'<div ico="cateringmngr_avtr"><div ico-obj1="cateringmngr_avtr"></div><div ico-obj2="cateringmngr_avtr"></div><div ico-obj3="cateringmngr_avtr"></div><div ico-obj4="cateringmngr_avtr"></div></div>',

eventmngr_avtr:'<div ico="eventmngr_avtr"><div ico-obj1="eventmngr_avtr"></div><div ico-obj2="eventmngr_avtr"></div></div>',

public_housemgt_avtr:'<div ico="public_housemgt_avtr"><div ico-obj1="public_housemgt_avtr"></div><div ico-obj2="public_housemgt_avtr"></div><div ico-obj2="public_housemgt_avtr"></div><div ico-obj3="public_housemgt_avtr"></div><div ico-obj4="public_housemgt_avtr"></div><div ico-obj5="public_housemgt_avtr"></div></div>',
//HOSPITALITY CATEGORY BEGINS HERE

//AVIATION AND AEROSPACE CATEGORY BEGINS HERE
aircabincrew_avtr:'<div ico="aircabincrew_avtr"><div ico-obj1="aircabincrew_avtr"></div><div ico-obj2="aircabincrew_avtr"></div><div ico-obj3="aircabincrew_avtr"></div></div>',

airlinepilot_avtr:'<div ico="airlinepilot_avtr"><div ico-obj2="airlinepilot_avtr"></div><div ico-obj1="airlinepilot_avtr"></div><div ico-obj3="airlinepilot_avtr"></div><div ico-obj4="airlinepilot_avtr"></div><div ico-obj5="airlinepilot_avtr"></div></div>',

airtraffictrl_avtr:'<div ico="airtraffictrl_avtr"><div ico-obj2="airtraffictrl_avtr"></div><div ico-obj1="airtraffictrl_avtr"></div><div ico-obj3="airtraffictrl_avtr"></div><div ico-obj4="airtraffictrl_avtr"></div><div ico-obj5="airtraffictrl_avtr"></div><div ico-obj6="airtraffictrl_avtr"></div></div>',

airhostest_avtr:'<div ico="airhostest_avtr"><div ico-obj1="airhostest_avtr"></div><div ico-obj2="airhostest_avtr"></div><div ico-obj3="airhostest_avtr"></div><div ico-obj4="airhostest_avtr"></div><div ico-obj5="airhostest_avtr"></div></div>',
//AVIATION AND AEROSPACE CATEGORY ENDS HERE

//AGRICULTURE CATEGORY BEGINS HERE
animalnutrition_avtr:'<div ico="animnutritn_avtr"><div ico-obj1="animnutritn_avtr"></div><div ico-obj2="animnutritn_avtr"></div><div ico-obj3="animnutritn_avtr"></div><div ico-obj4="animnutritn_avtr"></div></div>',

animaltech_avtr:'<div ico="animaltech_avtr"><div ico-obj1="animaltech_avtr"></div><div ico-obj2="animaltech_avtr"></div><div ico-obj3="animaltech_avtr"></div><div ico-obj4="animaltech_avtr"></div><div ico-obj5="animaltech_avtr"></div><div ico-obj6="animaltech_avtr"></div><div ico-obj7="animaltech_avtr"></div><div ico-obj8="animaltech_avtr"></div></div>',

diary_avtr:'<div ico="diary_avtr"><div ico-obj1="diary_avtr"></div><div ico-obj2="diary_avtr"></div><div ico-obj3="diary_avtr"></div><div ico-obj4="diary_avtr"></div></div>',

fishfarmmgt_avtr:'<div ico="fishfarmmgt_avtr"><div ico-obj1="fishfarmmgt_avtr"></div><div ico-obj2="fishfarmmgt_avtr"><div ico-obj3="fishfarmmgt_avtr"><div ico-obj4="fishfarmmgt_avtr"><div ico-obj5="fishfarmmgt_avtr"></div>',

nutritionist_avtr:'<div ico="nutritionist_avtr"><div ico-obj1="nutritionist_avtr"></div><div ico-obj2="nutritionist_avtr"></div><div ico-obj3="nutritionist_avtr"></div></div>',

agronomy_avtr:'<div ico="agronomy_avtr"><div ico-obj1="agronomy_avtr"></div><div ico-obj2="agronomy_avtr"></div><div ico-obj3="agronomy_avtr"></div></div>',

soilscientist_avtr:'<div ico="soilscientist_avtr"><div ico-obj1="soilscientist_avtr"></div><div ico-obj2="soilscientist_avtr"></div><div ico-obj3="soilscientist_avtr"></div><div ico-obj4="soilscientist_avtr"></div></div>',

foodproduction_avtr:'<div ico="foodprod_avtr"><div ico-obj1="foodprod_avtr"></div><div ico-obj2="foodprod_avtr"></div></div>',

cropscientist_avtr:'<div ico="cropsci_avtr"><div ico-obj1="cropsci_avtr"></div><div ico-obj2="cropsci_avtr"></div><div ico-obj3="cropsci_avtr"></div><div ico-obj4="cropsci_avtr"></div></div>',

arboriculture_avtr:'<div ico="arboriculture_avtr"><div ico-obj1="arboriculture_avtr"></div><div ico-obj2="arboriculture_avtr"></div></div>',

fishery_avtr:'<div ico="fishery_avtr"><div ico-obj1="fishery_avtr"></div></div>',

horticulture_avtr:'<div ico="horticulture_avtr"><div ico-obj1="horticulture_avtr"></div><div ico-obj2="horticulture_avtr"></div></div>',

apiculturalsci_avtr:'<div ico="apiculturalsci_avtr"><div ico-obj1="apiculturalsci_avtr"></div><div ico-obj2="apiculturalsci_avtr"></div><div ico-obj3="apiculturalsci_avtr"></div></div>',

animalhusbandary_avtr:'<div ico="animalhusbandary_avtr"><div ico-obj1="animalhusbandary_avtr"></div><div ico-obj2="animalhusbandary_avtr"></div><div ico-obj3="animalhusbandary_avtr"></div><div ico-obj4="animalhusbandary_avtr"></div><div ico-obj5="animalhusbandary_avtr"></div></div>',
//AGRICULTURE CATEGORY BEGINS HERE

//ENGINEERING CATEGORY BEGINS HERE
broadcastengr_avtr:'<div ico="broadcastengr_avtr"><div ico-obj1="broadcastengr_avtr"></div><div ico-obj2="broadcastengr_avtr"></div><div ico-obj3="broadcastengr_avtr"></div><div ico-obj4="broadcastengr_avtr"></div><div ico-obj5="broadcastengr_avtr"></div><div ico-obj6="broadcastengr_avtr"></div><div ico-obj7="broadcastengr_avtr"></div><div ico-obj8="broadcastengr_avtr"></div><div ico-obj9="broadcastengr_avtr"></div></div>',

chemicalengr_avtr:'<div ico="chemengr_avtr"><div ico-obj1="chemengr_avtr"></div><div ico-obj2="chemengr_avtr"></div><div ico-obj3="chemengr_avtr"></div><div ico-obj4="chemengr_avtr"></div><div ico-obj5="chemengr_avtr"></div><div ico-obj5="chemengr_avtr"></div><div ico-obj6="chemengr_avtr"></div><div ico-obj7="chemengr_avtr"></div><div ico-obj8="chemengr_avtr"></div><div ico-obj9="chemengr_avtr"></div></div>',

commengr_avtr:'<div ico="commengr_avtr"><div ico-obj1="commengr_avtr"></div><div ico-obj2="commengr_avtr"></div><div ico-obj3="commengr_avtr"></div><div ico-obj4="commengr_avtr"></div><div ico-obj5="commengr_avtr"></div><div ico-obj6="commengr_avtr"></div><div ico-obj7="commengr_avtr"></div><div ico-obj8="commengr_avtr"></div></div>',

drillingengr_avtr:'<div ico="drillinengr_avtr"><div ico-obj1="drillinengr_avtr"></div><div ico-obj2="drillinengr_avtr"></div><div ico-obj3="drillinengr_avtr"></div><div ico-obj4="drillinengr_avtr"></div><div ico-obj5="drillinengr_avtr"></div><div ico-obj6="drillinengr_avtr"></div></div>',

miningengr_avtr:'<div ico="mininengr_avtr"><div ico-obj1="mininengr_avtr"></div><div ico-obj2="mininengr_avtr"></div><div ico-obj3="mininengr_avtr"></div><div ico-obj4="mininengr_avtr"></div><div ico-obj5="mininengr_avtr"></div><div ico-obj6="mininengr_avtr"></div></div>',

mechengr_avtr:'<div ico="mechengr_avtr"><div ico-obj1="mechengr_avtr"></div><div ico-obj2="mechengr_avtr"></div><div ico-obj3="mechengr_avtr"></div><div ico-obj4="mechengr_avtr"></div><div ico-obj5="mechengr_avtr"></div><div ico-obj6="mechengr_avtr"></div><div ico-obj7="mechengr_avtr"></div><div ico-obj8="mechengr_avtr"></div></div>',

manufacturingsysengr_avtr:'<div ico="manufacturingsysengr_avtr"><div ico-obj1="manufacturingsysengr_avtr"></div><div ico-obj2="manufacturingsysengr_avtr"></div><div ico-obj3="manufacturingsysengr_avtr"></div><div ico-obj4="manufacturingsysengr_avtr"></div></div>',

maintenancengr_avtr:'<div ico="maintenancengr_avtr"><div ico-obj1="maintenancengr_avtr"></div><div ico-obj2="maintenancengr_avtr"></div><div ico-obj3="maintenancengr_avtr"></div><div ico-obj4="maintenancengr_avtr"></div><div ico-obj5="maintenancengr_avtr"></div><div ico-obj6="maintenancengr_avtr"></div><div ico-obj7="maintenancengr_avtr"></div></div>',

electricalengr_avtr:'<div ico="electricalengr_avtr"><div ico-obj1="electricalengr_avtr"></div><div ico-obj2="electricalengr_avtr"></div><div ico-obj3="electricalengr_avtr"></div><div ico-obj4="electricalengr_avtr"></div><div ico-obj5="electricalengr_avtr"></div><div ico-obj6="electricalengr_avtr"></div><div ico-obj7="electricalengr_avtr"></div></div>',

networkengr_avtr:'<div ico="networkengr_avtr"><div ico-obj1="networkengr_avtr"></div><div ico-obj2="networkengr_avtr"></div><div ico-obj3="networkengr_avtr"></div><div ico-obj4="networkengr_avtr"></div><div ico-obj5="networkengr_avtr"></div><div ico-obj6="networkengr_avtr"></div><div ico-obj7="networkengr_avtr"></div><div ico-obj8="networkengr_avtr"></div></div>',

petroleumengr_avtr:'<div ico="petroleumengr_avtr"><div ico-obj1="petroleumengr_avtr"></div><div ico-obj2="petroleumengr_avtr"></div><div ico-obj3="petroleumengr_avtr"></div><div ico-obj4="petroleumengr_avtr"></div></div>',

structural_avtr:'<div ico="structural_avtr"><div ico-obj1="structural_avtr"></div><div ico-obj2="structural_avtr"></div><div ico-obj3="structural_avtr"></div><div ico-obj4="structural_avtr"></div><div ico-obj5="structural_avtr"></div><div ico-obj6="structural_avtr"></div><div ico-obj7="structural_avtr"></div><div ico-obj8="structural_avtr"></div></div>',

sysengr_avtr:'<div ico="sysengr_avtr"><div ico-obj1="sysengr_avtr"></div><div ico-obj2="sysengr_avtr"></div><div ico-obj3="sysengr_avtr"></div><div ico-obj4="sysengr_avtr"></div><div ico-obj5="sysengr_avtr"></div></div>',

soundaudioengr_avtr:'<div ico="soundaudioengr_avtr"><div ico-obj1="soundaudioengr_avtr"></div><div ico-obj2="soundaudioengr_avtr"></div><div ico-obj3="soundaudioengr_avtr"></div><div ico-obj4="soundaudioengr_avtr"></div><div ico-obj5="soundaudioengr_avtr"></div><div ico-obj6="soundaudioengr_avtr"></div><div ico-obj7="soundaudioengr_avtr"></div><div ico-obj8="soundaudioengr_avtr"></div></div>',

railrdmanufacture_avtr:'<div ico="railrdmanufacture_avtr"><div ico-obj1="railrdmanufacture_avtr"></div><div ico-obj2="railrdmanufacture_avtr"></div><div ico-obj3="railrdmanufacture_avtr"></div></div>',

civilengr_avtr:'<div ico="civilengr_avtr"><div ico-obj1="civilengr_avtr"></div><div ico-obj2="civilengr_avtr"></div><div ico-obj3="civilengr_avtr"></div><div ico-obj4="civilengr_avtr"></div><div ico-obj5="civilengr_avtr"></div><div ico-obj6="civilengr_avtr"></div></div>',

prodengr_avtr:'<div ico="prodengr_avtr"><div ico-obj1="prodengr_avtr"></div><div ico-obj2="prodengr_avtr"></div><div ico-obj3="prodengr_avtr"></div><div ico-obj4="prodengr_avtr"></div><div ico-obj5="prodengr_avtr"></div><div ico-obj6="prodengr_avtr"></div></div>',

acousticengr_avtr:'<div ico="acousticengr_avtr"><div ico-obj1="acousticengr_avtr"></div><div ico-obj2="acousticengr_avtr"></div><div ico-obj3="acousticengr_avtr"></div><div ico-obj4="acousticengr_avtr"></div><div ico-obj5="acousticengr_avtr"></div><div ico-obj6="acousticengr_avtr"></div><div ico-obj7="acousticengr_avtr"></div><div ico-obj8="acousticengr_avtr"></div></div>',

electmanufacturing_avtr:'<div ico="electmanufacturing_avtr"><div ico-obj1="electmanufacturing_avtr"></div><div ico-obj2="electmanufacturing_avtr"></div></div>',

agricengr_avtr:'<div ico="agricengr_avtr"><div ico-obj1="agricengr_avtr"></div><div ico-obj2="agricengr_avtr"></div><div ico-obj3="agricengr_avtr"></div><div ico-obj4="agricengr_avtr"></div><div ico-obj5="agricengr_avtr"></div><div ico-obj6="agricengr_avtr"></div></div>',

aeronauticalengr_avtr:'<div ico="aeronauticalengr_avtr"><div ico-obj1="aeronauticalengr_avtr"></div><div ico-obj2="aeronauticalengr_avtr"></div><div ico-obj3="aeronauticalengr_avtr"></div><div ico-obj4="aeronauticalengr_avtr"></div><div ico-obj5="aeronauticalengr_avtr"></div></div>',

automotiveengr_avtr:'<div ico="automotiveengr_avtr"><div ico-obj1="automotiveengr_avtr"></div><div ico-obj2="automotiveengr_avtr"></div><div ico-obj3="automotiveengr_avtr"></div><div ico-obj3="automotiveengr_avtr"></div><div ico-obj4="automotiveengr_avtr"></div><div ico-obj5="automotiveengr_avtr"></div><div ico-obj6="automotiveengr_avtr"></div><div ico-obj7="automotiveengr_avtr"></div><div ico-obj8="automotiveengr_avtr"></div></div>',
//ENGINEERING CATEGORY ENDS HERE

//EDUCATION CATEGORY BEGINS HERE
edumgnt_avtr:'<div ico="edumgnt_avtr"><div ico-obj1="edumgnt_avtr"></div><div ico-obj2="edumgnt_avtr"></div><div ico-obj2="edumgnt_avtr"></div><div ico-obj3="edumgnt_avtr"></div><div ico-obj4="edumgnt_avtr"></div><div ico-obj5="edumgnt_avtr"></div><div ico-obj6="edumgnt_avtr"></div><div ico-obj7="edumgnt_avtr"></div></div>',

eduadmin_avtr:'<div ico="eduadmin_avtr"><div ico-obj1="eduadmin_avtr"></div><div ico-obj2="eduadmin_avtr"></div><div ico-obj3="eduadmin_avtr"></div><div ico-obj4="eduadmin_avtr"></div><div ico-obj4="eduadmin_avtr"></div><div ico-obj5="eduadmin_avtr"></div><div ico-obj6="eduadmin_avtr"></div><div ico-obj7="eduadmin_avtr"></div><div ico-obj8="eduadmin_avtr"></div></div>',

higheredu_avtr:'<div ico="higheredu_avtr"><div ico-obj1="higheredu_avtr"></div><div ico-obj2="higheredu_avtr"></div><div ico-obj3="higheredu_avtr"></div><div ico-obj4="higheredu_avtr"></div><div ico-obj5="higheredu_avtr"></div><div ico-obj6="higheredu_avtr"></div><div ico-obj7="higheredu_avtr"></div></div>',

liberian_avtr:'<div ico="liberian_avtr"><div ico-obj1="liberian_avtr"></div><div ico-obj2="liberian_avtr"></div><div ico-obj3="liberian_avtr"></div><div ico-obj4="liberian_avtr"></div><div ico-obj5="liberian_avtr"></div><div ico-obj6="liberian_avtr"></div><div ico-obj7="liberian_avtr"></div></div>',

pri_sec_avtr:'<div ico="pri_sec_avtr"><div ico-obj1="pri_sec_avtr"></div><div ico-obj2="pri_sec_avtr"></div><div ico-obj2="pri_sec_avtr"></div><div ico-obj3="pri_sec_avtr"></div><div ico-obj4="pri_sec_avtr"></div></div>',

specialedu_avtr:'<div ico="specialedu_avtr"><div ico-obj1="specialedu_avtr"></div><div ico-obj2="specialedu_avtr"></div><div ico-obj3="specialedu_avtr"></div><div ico-obj4="specialedu_avtr"></div><div ico-obj5="specialedu_avtr"></div><div ico-obj6="specialedu_avtr"></div></div>',
//EDUCATION CATEGORY ENDS HERE

//SPORTS CATEGORY BEGINS HERE
sportcoach_avtr:'<div ico="sportcoach_avtr"><div ico-obj1="sportcoach_avtr"></div></div>',

sport_avtr:'<div ico="sport_avtr"><div ico-obj1="sport_avtr"></div><div ico-obj2="sport_avtr"></div><div ico-obj3="sport_avtr"></div><div ico-obj4="sport_avtr"></div><div ico-obj5="sport_avtr"></div></div>',

umpire_ref_avtr:'<div ico="umpire_ref_avtr"><div ico-obj1="umpire_ref_avtr"></div><div ico-obj2="umpire_ref_avtr"></div></div>',
//SPORTS CATEGORY BEGINS HERE

//TRAVELS CATEGORY BEGINS HERE
tourguide_avtr:'<div ico="tourguide_avtr"><div ico-obj1="tourguide_avtr"></div><div ico-obj2="tourguide_avtr"></div><div ico-obj3="tourguide_avtr"></div><div ico-obj4="tourguide_avtr"></div></div>',

travelagent_avtr:'<div ico="travelagent_avtr"><div ico-obj1="travelagent_avtr"></div><div ico-obj2="travelagent_avtr"></div><div ico-obj3="travelagent_avtr"></div><div ico-obj4="travelagent_avtr"></div><div ico-obj5="travelagent_avtr"></div></div>',

tourism_avtr:'<div ico="tourism_avtr"><div ico-obj1="tourism_avtr"><div ico-obj2="tourism_avtr"></div></div><div ico-obj3="tourism_avtr"></div><div ico-obj4="tourism_avtr"></div></div>',

recreation_avtr:'<div ico="recreation_avtr"><div ico-obj1="recreation_avtr"></div><div ico-obj2="recreation_avtr"></div><div ico-obj3="recreation_avtr"></div></div>',

strategicplanin_avtr:'<div ico="strategicplanin_avtr"><div ico-obj1="strategicplanin_avtr"></div><div ico-obj2="strategicplanin_avtr"></div><div ico-obj3="strategicplanin_avtr"><div ico-obj4="strategicplanin_avtr"></div></div><div ico-obj5="strategicplanin_avtr"></div></div>',
//TRAVELS CATEGORY ENDS HERE

//SECURITY/DEFENSE CATEGORY BEGINS HERE
armedflsupport_avtr:'<div ico="armedflsupport_avtr"><div ico-obj1="armedflsupport_avtr"></div><div ico-obj2="armedflsupport_avtr"></div><div ico-obj3="armedflsupport_avtr"></div><div ico-obj4="armedflsupport_avtr"></div><div ico-obj5="armedflsupport_avtr"></div><div ico-obj6="armedflsupport_avtr"></div></div>',

armedfoprofficer_avtr:'<div ico="armedfoprofficer_avtr"><div ico-obj1="armedfoprofficer_avtr"></div><div ico-obj2="armedfoprofficer_avtr"></div><div ico-obj3="armedfoprofficer_avtr"></div><div ico-obj4="armedfoprofficer_avtr"></div><div ico-obj5="armedfoprofficer_avtr"></div><div ico-obj6="armedfoprofficer_avtr"></div><div ico-obj7="armedfoprofficer_avtr"></div><div ico-obj8="armedfoprofficer_avtr"></div></div>',

armedftechofficer_avtr:'<div ico="armedftechofficer_avtr"><div ico-obj1="armedftechofficer_avtr"></div><div ico-obj2="armedftechofficer_avtr"></div><div ico-obj3="armedftechofficer_avtr"></div><div ico-obj4="armedftechofficer_avtr"></div><div ico-obj5="armedftechofficer_avtr"></div><div ico-obj6="armedftechofficer_avtr"></div><div ico-obj7="armedftechofficer_avtr"></div></div>',

armedfedutrainingofficer_avtr:'<div ico="armedfedutrainingofficer_avtr"><div ico-obj1="armedfedutrainingofficer_avtr"></div><div ico-obj2="armedfedutrainingofficer_avtr"></div><div ico-obj2="armedfedutrainingofficer_avtr"></div><div ico-obj3="armedfedutrainingofficer_avtr"></div><div ico-obj4="armedfedutrainingofficer_avtr"></div><div ico-obj5="armedfedutrainingofficer_avtr"></div><div ico-obj6="armedfedutrainingofficer_avtr"></div><div ico-obj7="armedfedutrainingofficer_avtr"></div></div>',

lawenforceagent_avtr:'<div ico="lawenforceagent_avtr"><div ico-obj1="lawenforceagent_avtr"></div><div ico-obj2="lawenforceagent_avtr"></div><div ico-obj3="lawenforceagent_avtr"></div><div ico-obj4="lawenforceagent_avtr"></div><div ico-obj5="lawenforceagent_avtr"></div></div>',

military_avtr:'<div ico="military_avtr"><div ico-obj1="military_avtr"></div><div ico-obj2="military_avtr"></div><div ico-obj3="military_avtr"></div><div ico-obj4="military_avtr"></div><div ico-obj5="military_avtr"></div></div>',

prisonofficer_avtr:'<div ico="prisonofficer_avtr"><div ico-obj1="prisonofficer_avtr"></div><div ico-obj2="prisonofficer_avtr"></div><div ico-obj3="prisonofficer_avtr"></div><div ico-obj4="prisonofficer_avtr"></div><div ico-obj5="prisonofficer_avtr"></div><div ico-obj6="prisonofficer_avtr"></div><div ico-obj7="prisonofficer_avtr"></div><div ico-obj8="prisonofficer_avtr"></div></div>',

securityguard_avtr:'<div ico="securityguard_avtr"><div ico-obj1="securityguard_avtr"></div><div ico-obj2="securityguard_avtr"></div><div ico-obj3="securityguard_avtr"></div><div ico-obj4="securityguard_avtr"></div><div ico-obj5="securityguard_avtr"></div><div ico-obj6="securityguard_avtr"></div><div ico-obj7="securityguard_avtr"></div><div ico-obj8="securityguard_avtr"></div></div>',

defensespace_avtr:'<div ico="defensespace_avtr"><div ico-obj1="defensespace_avtr"></div></div>',

maritime_avtr:'<div ico="maritime_avtr"><div ico-obj1="maritime_avtr"></div><div ico-obj2="maritime_avtr"></div><div ico-obj3="maritime_avtr"></div><div ico-obj4="maritime_avtr"></div></div>',

primitivedetective_avtr:'<div ico="primitivedetective_avtr"><div ico-obj1="primitivedetective_avtr"></div><div ico-obj2="primitivedetective_avtr"></div><div ico-obj3="primitivedetective_avtr"></div><div ico-obj4="primitivedetective_avtr"></div><div ico-obj5="primitivedetective_avtr"></div><div ico-obj6="primitivedetective_avtr"></div><div ico-obj7="primitivedetective_avtr"></div></div>',

firefighter_avtr:'<div ico="firefighter_avtr"><div ico-obj1="firefighter_avtr"></div><div ico-obj2="firefighter_avtr"></div><div ico-obj3="firefighter_avtr"></div></div>',

intelofficer_avtr:'<div ico="intelofficer_avtr"><div ico-obj1="intelofficer_avtr"></div><div ico-obj2="intelofficer_avtr"></div><div ico-obj3="intelofficer_avtr"></div><div ico-obj4="intelofficer_avtr"></div><div ico-obj5="intelofficer_avtr"></div><div ico-obj6="intelofficer_avtr"></div><div ico-obj7="intelofficer_avtr"></div></div>',
//SECURITY/DEFENSE CATEGORY ENDS HERE

//COMMUNICATION CATEGORY BEGINS HERE
broadcastpresenter_avtr:'<div ico="broadcastpresenter_avtr"><div ico-obj1="broadcastpresenter_avtr"></div><div ico-obj2="broadcastpresenter_avtr"></div></div>',

newscaster_avtr:'<div ico="newscaster_avtr"><div ico-obj1="newscaster_avtr"></div><div ico-obj2="newscaster_avtr"></div><div ico-obj3="newscaster_avtr"></div><div ico-obj4="newscaster_avtr"></div><div ico-obj5="newscaster_avtr"></div></div>',

marketingadvert_avtr:'<div ico="marketingadvert_avtr"><div ico-obj1="marketingadvert_avtr"></div><div ico-obj2="marketingadvert_avtr"></div><div ico-obj3="marketingadvert_avtr"></div></div>',

newspapers_avtr:'<div ico="newspapers_avtr"><div ico-obj1="newspapers_avtr"></div><div ico-obj2="newspapers_avtr"></div><div ico-obj3="newspapers_avtr"></div><div ico-obj4="newspapers_avtr"></div></div>',

journalism_avtr:'<div ico="journalism_avtr"><div ico-obj1="journalism_avtr"></div><div ico-obj2="journalism_avtr"></div><div ico-obj3="journalism_avtr"></div><div ico-obj4="journalism_avtr"></div></div>',

publicrelcomm_avtr:'<div ico="publicrelcomm_avtr"><div ico-obj1="publicrelcomm_avtr"></div><div ico-obj2="publicrelcomm_avtr"></div><div ico-obj3="publicrelcomm_avtr"></div></div>',

salesprom_avtr:'<div ico="salesprom_avtr"><div ico-obj1="salesprom_avtr"></div><div ico-obj2="salesprom_avtr"></div><div ico-obj3="salesprom_avtr"></div></div>',

telecomss_avtr:'<div ico="telecomss_avtr"><div ico-obj1="telecomss_avtr"></div><div ico-obj2="telecomss_avtr"></div></div>',

translator_avtr:'<div ico="translator_avtr"><div ico-obj1="translator_avtr"></div><div ico-obj2="translator_avtr"></div><div ico-obj3="translator_avtr"></div><div ico-obj4="translator_avtr"></div><div ico-obj5="translator_avtr"></div><div ico-obj6="translator_avtr"></div></div>',
//COMMUNICATION CATEGORY BEGINS HERE

//MEDIA CATEGORY BEGINS HERE
photography_avtr:'<div ico="photography_avtr"><div ico-obj1="photography_avtr"></div><div ico-obj2="photography_avtr"></div><div ico-obj3="photography_avtr"></div></div>',

filmvideditor_avtr:'<div ico="filmvideditor_avtr"><div ico-obj1="filmvideditor_avtr"></div><div ico-obj2="filmvideditor_avtr"></div><div ico-obj3="filmvideditor_avtr"></div><div ico-obj4="filmvideditor_avtr"></div><div ico-obj5="filmvideditor_avtr"></div><div ico-obj6="filmvideditor_avtr"></div></div>',

mediaprod_avtr:'<div ico="mediaprod_avtr"><div ico-obj1="mediaprod_avtr"></div><div ico-obj2="mediaprod_avtr"></div><div ico-obj3="mediaprod_avtr"></div><div ico-obj4="mediaprod_avtr"></div><div ico-obj5="mediaprod_avtr"></div></div>',

mediabuying_avtr:'<div ico="mediabuying_avtr"><div ico-obj1="mediabuying_avtr"></div><div ico-obj2="mediabuying_avtr"></div></div>',

mediaplaning_avtr:'<div ico="mediaplaning_avtr"><div ico-obj1="mediaplaning_avtr"></div><div ico-obj2="mediaplaning_avtr"></div></div>',

motionpicfilm_avtr:'<div ico="motionpicfilm_avtr"><div ico-obj1="motionpicfilm_avtr"></div><div ico-obj2="motionpicfilm_avtr"></div><div ico-obj2="motionpicfilm_avtr"></div><div ico-obj3="motionpicfilm_avtr"></div><div ico-obj4="motionpicfilm_avtr"></div></div>',

multimediaprograma_avtr:'<div ico="multimediaprograma_avtr"><div ico-obj1="multimediaprograma_avtr"></div><div ico-obj2="multimediaprograma_avtr"></div><div ico-obj3="multimediaprograma_avtr"></div><div ico-obj4="multimediaprograma_avtr"></div><div ico-obj5="multimediaprograma_avtr"></div><div ico-obj6="multimediaprograma_avtr"></div></div>',

animation_avtr:'<div ico="animation_avtr"><div ico-obj1="animation_avtr"></div><div ico-obj2="animation_avtr"></div><div ico-obj3="animation_avtr"></div><div ico-obj4="animation_avtr"></div><div ico-obj5="animation_avtr"></div></div>',

planing_avtr:'<div ico="planing_avtr"><div ico-obj1="planing_avtr"></div><div ico-obj2="planing_avtr"></div><div ico-obj3="planing_avtr"></div></div>',
//MEDIA CATEGORY ENDS HERE

//FINANCE AND INVESTMENT CATEGORY BEGINS HERE
banking_avtr:'<div ico="banking_avtr"><div ico-obj1="banking_avtr"></div><div ico-obj2="banking_avtr"></div><div ico-obj3="banking_avtr"></div></div>',

investmntbnking_avtr:'<div ico="investmntbnking_avtr"><div ico-obj1="investmntbnking_avtr"></div><div ico-obj2="investmntbnking_avtr"></div><div ico-obj3="investmntbnking_avtr"></div></div>',

finservices_avtr:'<div ico="finservices_avtr"><div ico-obj1="finservices_avtr"></div><div ico-obj2="finservices_avtr"></div><div ico-obj3="finservices_avtr"></div><div ico-obj4="finservices_avtr"></div><div ico-obj5="finservices_avtr"></div></div>',

finriskanalyst_avtr:'<div ico="finriskanalyst_avtr"><div ico-obj1="finriskanalyst_avtr"></div><div ico-obj2="finriskanalyst_avtr"></div><div ico-obj3="finriskanalyst_avtr"></div><div ico-obj4="finriskanalyst_avtr"></div><div ico-obj5="finriskanalyst_avtr"></div></div>',

bondstrader_avtr:'<div ico="bondstrader_avtr"><div ico-obj1="bondstrader_avtr"></div><div ico-obj2="bondstrader_avtr"></div><div ico-obj3="bondstrader_avtr"></div><div ico-obj4="bondstrader_avtr"></div><div ico-obj5="bondstrader_avtr"></div><div ico-obj6="bondstrader_avtr"></div></div>',

fundraising_avtr:'<div ico="fundraising_avtr"><div ico-obj1="fundraising_avtr"></div><div ico-obj2="fundraising_avtr"></div><div ico-obj3="fundraising_avtr"></div><div ico-obj4="fundraising_avtr"></div></div>',

insurance_avtr:'<div ico="insurance_avtr"><div ico-obj1="insurance_avtr"></div><div ico-obj2="insurance_avtr"></div><div ico-obj3="insurance_avtr"></div></div>',

investmentanalyst_avtr:'<div ico="investmentanalyst_avtr"><div ico-obj1="investmentanalyst_avtr"></div><div ico-obj2="investmentanalyst_avtr"></div><div ico-obj3="investmentanalyst_avtr"></div></div>',

investmentmgt_avtr:'<div ico="investmentmgt_avtr"><div ico-obj1="investmentmgt_avtr"></div><div ico-obj2="investmentmgt_avtr"></div><div ico-obj3="investmentmgt_avtr"></div></div>',

fineconomist_avtr:'<div ico="fineconomist_avtr"><div ico-obj1="fineconomist_avtr"></div><div ico-obj2="fineconomist_avtr"></div><div ico-obj3="fineconomist_avtr"></div><div ico-obj4="fineconomist_avtr"></div><div ico-obj5="fineconomist_avtr"></div><div ico-obj6="fineconomist_avtr"></div></div>',

capitalmarket_avtr:'<div ico="capitalmarket_avtr"><div ico-obj1="capitalmarket_avtr"></div><div ico-obj2="capitalmarket_avtr"></div><div ico-obj3="capitalmarket_avtr"></div></div>',
//FINANCE AND INVESTMENT CATEGORY ENDS HERE

//HEALTH AND MEDICARE CATEGORY BEGINS HERE
anatomy_avtr:'<div ico="anatomy_avtr"><div ico-obj1="anatomy_avtr"></div><div ico-obj2="anatomy_avtr"></div><div ico-obj3="anatomy_avtr"></div><div ico-obj4="anatomy_avtr"></div></div>',

altmedicine_avtr:'<div ico="altmedicine_avtr"><div ico-obj1="altmedicine_avtr"></div><div ico-obj2="altmedicine_avtr"></div></div>',

chiropodist_avtr:'<div ico="chiropodist_avtr"><div ico-obj1="chiropodist_avtr"></div><div ico-obj2="chiropodist_avtr"></div><div ico-obj3="chiropodist_avtr"></div></div>',

dentist_avtr:'<div ico="dentist_avtr"><div ico-obj1="dentist_avtr"></div><div ico-obj2="dentist_avtr"></div><div ico-obj3="dentist_avtr"></div></div>',

hematology_avtr:'<div ico="hematology_avtr"><div ico-obj1="hematology_avtr"></div><div ico-obj2="hematology_avtr"></div><div ico-obj3="hematology_avtr"></div></div>',

hospitalhealthcare_avtr:'<div ico="hospitalhealthcare_avtr"><div ico-obj1="hospitalhealthcare_avtr"></div><div ico-obj2="hospitalhealthcare_avtr"></div></div>',

immunologist_avtr:'<div ico="immunologist_avtr"><div ico-obj1="immunologist_avtr"></div><div ico-obj2="immunologist_avtr"></div></div>',

medicarepractice_avtr:'<div ico="medicarepractice_avtr"><div ico-obj1="medicarepractice_avtr"></div><div ico-obj2="medicarepractice_avtr"></div><div ico-obj3="medicarepractice_avtr"></div></div>',

midwifery_avtr:'<div ico="midwifery_avtr"><div ico-obj1="midwifery_avtr"></div><div ico-obj2="midwifery_avtr"></div><div ico-obj3="midwifery_avtr"></div><div ico-obj4="midwifery_avtr"></div><div ico-obj5="midwifery_avtr"></div><div ico-obj6="midwifery_avtr"></div></div>',

surgeon_avtr:'<div ico="surgeon_avtr"><div ico-obj1="surgeon_avtr"></div><div ico-obj2="surgeon_avtr"></div><div ico-obj3="surgeon_avtr"></div><div ico-obj4="surgeon_avtr"></div><div ico-obj5="surgeon_avtr"></div></div>',

nurse_avtr:'<div ico="nurse_avtr"><div ico-obj1="nurse_avtr"></div><div ico-obj2="nurse_avtr"></div><div ico-obj3="nurse_avtr"></div><div ico-obj4="nurse_avtr"></div><div ico-obj5="nurse_avtr"></div><div ico-obj6="nurse_avtr"></div></div>',

menatalhealthcare_avtr:'<div ico="menatalhealthcare_avtr"><div ico-obj1="menatalhealthcare_avtr"></div><div ico-obj2="menatalhealthcare_avtr"></div><div ico-obj3="menatalhealthcare_avtr"></div><div ico-obj4="menatalhealthcare_avtr"></div></div>',

psychiatry_avtr:'<div ico="psychiatry_avtr"><div ico-obj1="psychiatry_avtr"></div><div ico-obj2="psychiatry_avtr"></div><div ico-obj3="psychiatry_avtr"></div><div ico-obj4="psychiatry_avtr"></div></div>',

veterinarydoc_surgeon_avtr:'<div ico="veterinarydoc_surgeon_avtr"><div ico-obj1="veterinarydoc_surgeon_avtr"></div><div ico-obj2="veterinarydoc_surgeon_avtr"></div><div ico-obj3="veterinarydoc_surgeon_avtr"></div><div ico-obj4="veterinarydoc_surgeon_avtr"></div></div>',

clinicalembryologist_avtr:'<div ico="clinicalembryologist_avtr"><div ico-obj1="clinicalembryologist_avtr"></div><div ico-obj2="clinicalembryologist_avtr"></div><div ico-obj3="clinicalembryologist_avtr"></div><div ico-obj4="clinicalembryologist_avtr"></div></div>',

clinicalnucleargenetics_avtr:'<div ico="clinicalnucleargenetics_avtr"><div ico-obj1="clinicalnucleargenetics_avtr"></div><div ico-obj2="clinicalnucleargenetics_avtr"></div><div ico-obj3="clinicalnucleargenetics_avtr"></div><div ico-obj4="clinicalnucleargenetics_avtr"></div></div>',

gyneocologist_avtr:'<div ico="gyneocologist_avtr"><div ico-obj1="gyneocologist_avtr"></div><div ico-obj2="gyneocologist_avtr"></div><div ico-obj3="gyneocologist_avtr"></div><div ico-obj4="gyneocologist_avtr"></div></div>',

radiographer_avtr:'<div ico="radiographer_avtr"><div ico-obj1="radiographer_avtr"></div><div ico-obj2="radiographer_avtr"></div><div ico-obj3="radiographer_avtr"></div><div ico-obj4="radiographer_avtr"></div></div>',

clinicalcytogenetic_avtr:'<div ico="clinicalcytogenetic_avtr"><div ico-obj1="clinicalcytogenetic_avtr"></div><div ico-obj2="clinicalcytogenetic_avtr"></div><div ico-obj3="clinicalcytogenetic_avtr"></div><div ico-obj4="clinicalcytogenetic_avtr"></div><div ico-obj5="clinicalcytogenetic_avtr"></div></div>',

ophthalmologist_avtr:'<div ico="ophthalmologist_avtr"><div ico-obj1="ophthalmologist_avtr"></div></div>',

pharmaceutical_avtr:'<div ico="pharmaceutical_avtr"><div ico-obj1="pharmaceutical_avtr"></div></div>',

medlab_avtr:'<div ico="medlab_avtr"><div ico-obj1="medlab_avtr"></div><div ico-obj2="medlab_avtr"></div><div ico-obj3="medlab_avtr"></div><div ico-obj4="medlab_avtr"></div></div>',

publichealth_avtr:'<div ico="publichealth_avtr"><div ico-obj1="publichealth_avtr"></div><div ico-obj2="publichealth_avtr"></div><div ico-obj3="publichealth_avtr"></div><div ico-obj4="publichealth_avtr"></div><div ico-obj5="publichealth_avtr"></div><div ico-obj6="publichealth_avtr"></div></div>',

environmentalhealth_avtr:'<div ico="environmentalhealth_avtr"><div ico-obj1="environmentalhealth_avtr"></div><div ico-obj2="environmentalhealth_avtr"></div><div ico-obj3="environmentalhealth_avtr"></div></div>',

commhealth_avtr:'<div ico="commhealth_avtr"><div ico-obj1="commhealth_avtr"></div><div ico-obj2="commhealth_avtr"></div><div ico-obj3="commhealth_avtr"></div><div ico-obj4="commhealth_avtr"></div><div ico-obj5="commhealth_avtr"></div><div ico-obj6="commhealth_avtr"></div><div ico-obj7="commhealth_avtr"></div><div ico-obj8="commhealth_avtr"></div><div ico-obj9="commhealth_avtr"></div></div>',

optometry_avtr:'<div ico="optometry_avtr"><div ico-obj1="optometry_avtr"></div></div>',

orthopist_avtr:'<div ico="orthopist_avtr"><div ico-obj1="orthopist_avtr"></div><div ico-obj2="orthopist_avtr"></div></div>',

pathologist_avtr:'<div ico="pathologist_avtr"><div ico-obj1="pathologist_avtr"></div><div ico-obj2="pathologist_avtr"></div></div>',

physiologist_avtr:'<div ico="physiologist_avtr"><div ico-obj1="physiologist_avtr"></div><div ico-obj2="physiologist_avtr"></div><div ico-obj3="physiologist_avtr"></div><div ico-obj4="physiologist_avtr"></div><div ico-obj5="physiologist_avtr"></div></div>',

biotechnology_avtr:'<div ico="biotechnology_avtr"><div ico-obj1="biotechnology_avtr"></div><div ico-obj2="biotechnology_avtr"></div></div>',
//HEALTH AND MEDICARE CATEGORY ENDS HERE

//PUBLIC SERVICE CATEGORY BEGINS HERE
civilsocialorg_avtr:'<div ico="civilsocialorg_avtr"><div ico-obj1="civilsocialorg_avtr"></div><div ico-obj2="civilsocialorg_avtr"></div><div ico-obj3="civilsocialorg_avtr"></div></div>',

civilservant_avtr:'<div ico="civilservant_avtr"><div ico-obj1="civilservant_avtr"></div><div ico-obj2="civilservant_avtr"></div><div ico-obj3="civilservant_avtr"></div><div ico-obj4="civilservant_avtr"></div><div ico-obj5="civilservant_avtr"></div></div>',

commedevworkers_avtr:'<div ico="commedevworkers_avtr"><div ico-obj1="commedevworkers_avtr"></div><div ico-obj2="commedevworkers_avtr"></div><div ico-obj3="commedevworkers_avtr"></div></div>',

govrel_avtr:'<div ico="govrel_avtr"><div ico-obj1="govrel_avtr"></div><div ico-obj2="govrel_avtr"></div><div ico-obj3="govrel_avtr"></div><div ico-obj4="govrel_avtr"></div></div>',

nonprofitorg_avtr:'<div ico="nonprofitorg_avtr"><div ico-obj1="nonprofitorg_avtr"></div><div ico-obj2="nonprofitorg_avtr"></div><div ico-obj3="nonprofitorg_avtr"></div></div>',

publicsafety_avtr:'<div ico="publicsafety_avtr"><div ico-obj1="publicsafety_avtr"></div><div ico-obj2="publicsafety_avtr"></div><div ico-obj3="publicsafety_avtr"></div><div ico-obj4="publicsafety_avtr"></div></div>',

politicalorg_avtr:'<div ico="politicalorg_avtr"><div ico-obj1="politicalorg_avtr"></div><div ico-obj2="politicalorg_avtr"></div><div ico-obj3="politicalorg_avtr"></div><div ico-obj4="politicalorg_avtr"></div><div ico-obj5="politicalorg_avtr"></div><div ico-obj6="politicalorg_avtr"></div></div>',

youthworker_avtr:'<div ico="youthworker_avtr"><div ico-obj1="youthworker_avtr"></div><div ico-obj2="youthworker_avtr"></div><div ico-obj3="youthworker_avtr"></div><div ico-obj4="youthworker_avtr"></div></div>',

socialworker_avtr:'<div ico="socialworker_avtr"><div ico-obj1="socialworker_avtr"></div><div ico-obj2="socialworker_avtr"></div><div ico-obj3="socialworker_avtr"></div><div ico-obj4="socialworker_avtr"></div><div ico-obj5="socialworker_avtr"></div><div ico-obj6="socialworker_avtr"></div></div>',

individualfamilyservice_avtr:'<div ico="individualfamilyservice_avtr"><div ico-obj1="individualfamilyservice_avtr"></div><div ico-obj2="individualfamilyservice_avtr"></div><div ico-obj3="individualfamilyservice_avtr"></div><div ico-obj4="individualfamilyservice_avtr"></div><div ico-obj5="individualfamilyservice_avtr"></div><div ico-obj6="individualfamilyservice_avtr"></div></div>',

legislativeoffice_avtr:'<div ico="legislativeoffice_avtr"><div ico-obj1="legislativeoffice_avtr"></div><div ico-obj2="legislativeoffice_avtr"></div><div ico-obj3="legislativeoffice_avtr"></div><div ico-obj4="legislativeoffice_avtr"></div><div ico-obj5="legislativeoffice_avtr"></div><div ico-obj6="legislativeoffice_avtr"></div></div>',
//PUBLIC SERVICE CATEGORY ENDS HERE

//INTERNET CATEGORY BEGINS HERE
elearning_avtr:'<div ico="elearning_avtr"><div ico-obj1="elearning_avtr"></div><div ico-obj2="elearning_avtr"></div><div ico-obj3="elearning_avtr"></div></div>',

socialmedia_avtr:'<div ico="socialmedia_avtr"><div ico-obj1="socialmedia_avtr"></div><div ico-obj2="socialmedia_avtr"></div><div ico-obj3="socialmedia_avtr"></div><div ico-obj4="socialmedia_avtr"></div><div ico-obj5="socialmedia_avtr"></div></div>',

onlinemedia_avtr:'<div ico="onlinemedia_avtr"><div ico-obj1="onlinemedia_avtr"></div><div ico-obj2="onlinemedia_avtr"></div></div>',
//INTERNET CATEGORY ENDS HERE

//JUDICIARY CATEGORY BEGINS HERE
juduciary_avtr:'<div ico="juduciary_avtr"><div ico-obj1="juduciary_avtr"></div><div ico-obj2="juduciary_avtr"></div><div ico-obj3="juduciary_avtr"></div></div>',

legalpractice_avtr:'<div ico="legalpractice_avtr"><div ico-obj1="legalpractice_avtr"></div><div ico-obj2="legalpractice_avtr"></div><div ico-obj3="legalpractice_avtr"></div><div ico-obj4="legalpractice_avtr"></div><div ico-obj5="legalpractice_avtr"></div><div ico-obj6="legalpractice_avtr"></div></div>',
//JUDICIARY CATEGORY ENDS HERE

//RELIGIOUS INSTITUTION CATEGORY BEGINS HERE
pastor_avtr:'<div ico="pastor_avtr"><div ico-obj1="pastor_avtr"></div><div ico-obj2="pastor_avtr"></div><div ico-obj3="pastor_avtr"></div><div ico-obj4="pastor_avtr"></div><div ico-obj5="pastor_avtr"></div><div ico-obj6="pastor_avtr"></div></div>',

imam_avtr:'<div ico="imam_avtr"><div ico-obj1="imam_avtr"></div><div ico-obj2="imam_avtr"></div><div ico-obj3="imam_avtr"></div><div ico-obj4="imam_avtr"></div><div ico-obj5="imam_avtr"></div></div>',

priest_avtr:'<div ico="priest_avtr"><div ico-obj1="priest_avtr"></div><div ico-obj2="priest_avtr"></div><div ico-obj3="priest_avtr"></div><div ico-obj4="priest_avtr"></div><div ico-obj5="priest_avtr"></div><div ico-obj6="priest_avtr"></div></div>',
//RELIGIOUS INSTITUTION CATEGORY BEGINS HERE

//HEALTH WELLNESS & FITNESS CATEGORY BEGINS HERE
healthphysiologist_avtr:'<div ico="healthphysiologist_avtr"><div ico-obj1="healthphysiologist_avtr"></div><div ico-obj2="healthphysiologist_avtr"></div></div>',

athletictrainer_avtr:'<div ico="athletictrainer_avtr"><div ico-obj1="athletictrainer_avtr"></div><div ico-obj2="athletictrainer_avtr"></div><div ico-obj3="athletictrainer_avtr"></div></div>',

exercissci_avtr:'<div ico="exercissci_avtr"><div ico-obj1="exercissci_avtr"></div><div ico-obj2="exercissci_avtr"></div></div>',

yogainstructor_avtr:'<div ico="yogainstructor_avtr"><div ico-obj1="yogainstructor_avtr"></div><div ico-obj2="yogainstructor_avtr"></div><div ico-obj3="yogainstructor_avtr"></div></div>',

fitnessmanager_avtr:'<div ico="fitnessmanager_avtr"><div ico-obj1="fitnessmanager_avtr"></div><div ico-obj2="fitnessmanager_avtr"></div><div ico-obj3="fitnessmanager_avtr"></div></div>',

lifestylecoach_avtr:'<div ico="lifestylecoach_avtr"><div ico-obj1="lifestylecoach_avtr"></div><div ico-obj2="lifestylecoach_avtr"></div><div ico-obj3="lifestylecoach_avtr"></div><div ico-obj4="lifestylecoach_avtr"></div><div ico-obj5="lifestylecoach_avtr"></div><div ico-obj6="lifestylecoach_avtr"></div></div>',

spaprof_avtr:'<div ico="spaprof_avtr"><div ico-obj1="spaprof_avtr"></div><div ico-obj2="spaprof_avtr"></div><div ico-obj3="spaprof_avtr"></div><div ico-obj4="spaprof_avtr"></div><div ico-obj5="spaprof_avtr"></div></div>',

indoorcycling_avtr:'<div ico="indoorcycling_avtr"><div ico-obj1="indoorcycling_avtr"></div><div ico-obj2="indoorcycling_avtr"></div><div ico-obj3="indoorcycling_avtr"></div><div ico-obj4="indoorcycling_avtr"></div></div>',

ballet_avtr:'<div ico="ballet_avtr"><div ico-obj1="ballet_avtr"></div><div ico-obj2="ballet_avtr"></div><div ico-obj3="ballet_avtr"></div><div ico-obj4="ballet_avtr"></div></div>',

kickboxing_avtr:'<div ico="kickboxing_avtr"><div ico-obj1="kickboxing_avtr"></div></div>',
//HEALTH WELLNESS & FITNESS CATEGORY ENDS HERE

//SCIENCE TECHNOLOGY BEGINS HERE
computerhardware_avtr:'<div ico="computerhardware_avtr"><div ico-obj1="computerhardware_avtr"></div></div>',

comptprogramer_avtr:'<div ico="comptprogramer_avtr"><div ico-obj1="comptprogramer_avtr"></div><div ico-obj2="comptprogramer_avtr"></div><div ico-obj3="comptprogramer_avtr"></div><div ico-obj4="comptprogramer_avtr"></div><div ico-obj5="comptprogramer_avtr"></div></div>',

databaseadmin_avtr:'<div ico="databaseadmin_avtr"><div ico-obj1="databaseadmin_avtr"></div><div ico-obj2="databaseadmin_avtr"></div><div ico-obj3="databaseadmin_avtr"></div><div ico-obj4="databaseadmin_avtr"></div><div ico-obj5="databaseadmin_avtr"></div></div>',

epidemiology_avtr:'<div ico="epidemiology_avtr"><div ico-obj1="epidemiology_avtr"></div><div ico-obj2="epidemiology_avtr"></div><div ico-obj3="epidemiology_avtr"></div><div ico-obj4="epidemiology_avtr"></div><div ico-obj5="epidemiology_avtr"></div><div ico-obj6="epidemiology_avtr"></div></div>',

foodtech_avtr:'<div ico="foodtech_avtr"><div ico-obj1="foodtech_avtr"></div><div ico-obj2="foodtech_avtr"></div></div>',

inforechservice_avtr:'<div ico="inforechservice_avtr"><div ico-obj1="inforechservice_avtr"></div><div ico-obj2="inforechservice_avtr"></div><div ico-obj3="inforechservice_avtr"></div><div ico-obj4="inforechservice_avtr"></div></div>',

microbiology_avtr:'<div ico="microbiology_avtr"><div ico-obj1="microbiology_avtr"></div><div ico-obj2="microbiology_avtr"></div><div ico-obj3="microbiology_avtr"></div></div>',

mycologist_avtr:'<div ico="mycologist_avtr"><div ico-obj1="mycologist_avtr"></div><div ico-obj2="mycologist_avtr"></div><div ico-obj3="mycologist_avtr"></divbj1="mycologist_></div>',

metalurgist_avtr:'<div ico="metalurgist_avtr"><div ico-obj1="metalurgist_avtr"></div><div ico-obj2="metalurgist_avtr"></div><div ico-obj3="metalurgist_avtr"></divbj1="metalurgist_avtr></div>',

meteorology_avtr:'<div ico="meteorology_avtr"><div ico-obj1="meteorology_avtr"></div></div>',

audiologicalsci_avtr:'<div ico="audiologicalsci_avtr"><div ico-obj1="audiologicalsci_avtr"></div><div ico-obj2="audiologicalsci_avtr"></div><div ico-obj3="audiologicalsci_avtr"></div><div ico-obj4="audiologicalsci_avtr"></div></div>',

clothingtextiletech_avtr:'<div ico="clothingtextiletech_avtr"><div ico-obj1="clothingtextiletech_avtr"></div></div>',

dietition_avtr:'<div ico="dietition_avtr"><div ico-obj1="dietition_avtr"></div><div ico-obj2="dietition_avtr"></div></div>',

geochemistry_avtr:'<div ico="geochemistry_avtr"><div ico-obj1="geochemistry_avtr"></div><div ico-obj2="geochemistry_avtr"></div></div>',

geology_avtr:'<div ico="geology_avtr"><div ico-obj1="geology_avtr"></div><div ico-obj2="geology_avtr"></div></div>',

zoology_avtr:'<div ico="zoology_avtr"><div ico-obj1="zoology_avtr"></div><div ico-obj2="zoology_avtr"></div><div ico-obj3="zoology_avtr"></div></div>',

indusrialchem_avtr:'<div ico="indusrialchem_avtr"><div ico-obj1="indusrialchem_avtr"></div><div ico-obj2="indusrialchem_avtr"></div></div>',

physicist_avtr:'<div ico="physicist_avtr"><div ico-obj1="physicist_avtr"></div><div ico-obj2="physicist_avtr"></div><div ico-obj3="physicist_avtr"></div><div ico-obj3="physicist_avtr"></div></div>',

marinescientist_avtr:'<div ico="marinescientist_avtr"><div ico-obj1="marinescientist_avtr"></div><div ico-obj2="marinescientist_avtr"></div><div ico-obj3="marinescientist_avtr"></div></div>',

statistics_avtr:'<div ico="statistics_avtr"><div ico-obj1="statistics_avtr"></div><div ico-obj2="statistics_avtr"></div></div>',

interactiondesign_avtr:'<div ico="interactiondesign_avtr"><div ico-obj1="interactiondesign_avtr"></div><div ico-obj2="interactiondesign_avtr"></div><div ico-obj3="interactiondesign_avtr"></div><div ico-obj4="interactiondesign_avtr"></div><div ico-obj5="interactiondesign_avtr"></div></div>',

industrialdesign_avtr:'<div ico="industrialdesign_avtr"><div ico-obj1="industrialdesign_avtr"></div><div ico-obj2="industrialdesign_avtr"></div><div ico-obj3="industrialdesign_avtr"></div><div ico-obj4="industrialdesign_avtr"></div><div ico-obj5="industrialdesign_avtr"></div></div>',

archeologist_avtr:'<div ico="archeologist_avtr"><div ico-obj1="archeologist_avtr"></div></div>',

cartography_avtr:'<div ico="cartography_avtr"><div ico-obj1="cartography_avtr"></div></div>',

forensicscientist_avtr:'<div ico="forensicscientist_avtr"><div ico-obj1="forensicscientist_avtr"></div><div ico-obj2="forensicscientist_avtr"></div></div>',

geophysis_avtr:'<div ico="geophysis_avtr"><div ico-obj1="geophysis_avtr"></div><div ico-obj2="geophysis_avtr"></div><div ico-obj3="geophysis_avtr"></div><div ico-obj4="geophysis_avtr"></div></div>',
//SCIENCE TECHNOLOGY ENDS HERE

//SERVICES/BUSINESS BEGINS HERE
barber_avtr:'<div ico="barber_avtr"><div ico-obj1="barber_avtr"></div><div ico-obj2="barber_avtr"></div><div ico-obj3="barber_avtr"></div></div>',

bookseller_avtr:'<div ico="bookseller_avtr"><div ico-obj1="bookseller_avtr"></div><div ico-obj2="bookseller_avtr"></div><div ico-obj3="bookseller_avtr"></div></div>',

cellcentermgr_avtr:'<div ico="cellcentermgr_avtr"><div ico-obj1="cellcentermgr_avtr"></div><div ico-obj2="cellcentermgr_avtr"></div><div ico-obj3="cellcentermgr_avtr"></div><div ico-obj4="cellcentermgr_avtr"></div></div>',

customercarerep_avtr:'<div ico="customercarerep_avtr"><div ico-obj1="customercarerep_avtr"></div><div ico-obj2="customercarerep_avtr"></div><div ico-obj3="customercarerep_avtr"></div><div ico-obj4="customercarerep_avtr"></div></div>',

salesrep_avtr:'<div ico="salesrep_avtr"><div ico-obj1="salesrep_avtr"></div><div ico-obj2="salesrep_avtr"></div><div ico-obj3="salesrep_avtr"></div><div ico-obj4="salesrep_avtr"></div><div ico-obj5="salesrep_avtr"></div></div>',

careerinfomgt_avtr:'<div ico="careerinfomgt_avtr"><div ico-obj1="careerinfomgt_avtr"></div><div ico-obj2="careerinfomgt_avtr"></div><div ico-obj3="careerinfomgt_avtr"></div></div>',

eventservices_avtr:'<div ico="eventservices_avtr"><div ico-obj1="eventservices_avtr"></div><div ico-obj2="eventservices_avtr"></div><div ico-obj3="eventservices_avtr"></div><div ico-obj4="eventservices_avtr"></div><div ico-obj5="eventservices_avtr"></div><div ico-obj6="eventservices_avtr"></div></div>',

facilitieservices_avtr:'<div ico="facilitieservices_avtr"><div ico-obj1="facilitieservices_avtr"></div><div ico-obj2="facilitieservices_avtr"></div><div ico-obj3="facilitieservices_avtr"></div></div>',

gamblincasino_avtr:'<div ico="gamblincasino_avtr"><div ico-obj1="gamblincasino_avtr"></div><div ico-obj2="gamblincasino_avtr"></div><div ico-obj3="gamblincasino_avtr"></div><div ico-obj4="gamblincasino_avtr"></div></div>',

hospitality_avtr:'<div ico="hospitality_avtr"><div ico-obj1="hospitality_avtr"></div><div ico-obj2="hospitality_avtr"></div><div ico-obj3="hospitality_avtr"></div></div>',

humanresource_avtr:'<div ico="humanresource_avtr"><div ico-obj1="humanresource_avtr"></div><div ico-obj2="humanresource_avtr"></div><div ico-obj3="humanresource_avtr"></div><div ico-obj4="humanresource_avtr"></div><div ico-obj5="humanresource_avtr"></div></div>',

importexport_avtr:'<div ico="importexport_avtr"><div ico-obj1="importexport_avtr"></div><div ico-obj2="importexport_avtr"></div></div>',

indivnfamservices_avtr:'<div ico="indivnfamservices_avtr"><div ico-obj1="indivnfamservices_avtr"></div><div ico-obj2="indivnfamservices_avtr"></div><div ico-obj3="indivnfamservices_avtr"></div></div>',

infoservice_avtr:'<div ico="infoservice_avtr"><div ico-obj1="infoservice_avtr"></div></div>',

logisticsupplychain_avtr:'<div ico="logisticsupplychain_avtr"><div ico-obj1="logisticsupplychain_avtr"></div><div ico-obj2="logisticsupplychain_avtr"></div><div ico-obj3="logisticsupplychain_avtr"></div><div ico-obj4="logisticsupplychain_avtr"></div><div ico-obj5="logisticsupplychain_avtr"></div></div>',

luxurygoodsjewlry_avtr:'<div ico="luxurygoodsjewlry_avtr"><div ico-obj1="luxurygoodsjewlry_avtr"></div><div ico-obj2="luxurygoodsjewlry_avtr"></div><div ico-obj3="luxurygoodsjewlry_avtr"></div><div ico-obj4="luxurygoodsjewlry_avtr"></div></div>',

outsourcingoffshore_avtr:'<div ico="outsourcingoffshore_avtr"><div ico-obj1="outsourcingoffshore_avtr"></div><div ico-obj2="outsourcingoffshore_avtr"></div></div>',

packagecontainers_avtr:'<div ico="packagecontainers_avtr"><div ico-obj1="packagecontainers_avtr"></div><div ico-obj2="packagecontainers_avtr"></div><div ico-obj3="packagecontainers_avtr"></div></div>',

paperforestprod_avtr:'<div ico="paperforestprod_avtr"><div ico-obj1="paperforestprod_avtr"></div><div ico-obj2="paperforestprod_avtr"></div><div ico-obj3="paperforestprod_avtr"></div></div>',

philanthropy_avtr:'<div ico="philanthropy_avtr"><div ico-obj1="philanthropy_avtr"></div><div ico-obj2="philanthropy_avtr"></div></div>',

cosmetics_avtr:'<div ico="cosmetics_avtr"><div ico-obj1="cosmetics_avtr"></div><div ico-obj2="cosmetics_avtr"></div></div>',

proftrainincoaching_avtr:'<div ico="proftrainincoaching_avtr"><div ico-obj1="proftrainincoaching_avtr"></div><div ico-obj2="proftrainincoaching_avtr"></div></div>',

restaurant_avtr:'<div ico="restaurant_avtr"><div ico-obj1="restaurant_avtr"></div><div ico-obj2="restaurant_avtr"></div><div ico-obj3="restaurant_avtr"></div></div>',

consumergoods_avtr:'<div ico="consumergoods_avtr"><div ico-obj1="consumergoods_avtr"></div><div ico-obj2="consumergoods_avtr"></div><div ico-obj3="consumergoods_avtr"></div></div>',

bizsupplierequip_avtr:'<div ico="bizsupplierequip_avtr"><div ico-obj1="bizsupplierequip_avtr"></div><div ico-obj2="bizsupplierequip_avtr"></div><div ico-obj3="bizsupplierequip_avtr"></div><div ico-obj4="bizsupplierequip_avtr"></div></div>',

merchandizing_avtr:'<div ico="merchandizing_avtr"><div ico-obj1="merchandizing_avtr"></div><div ico-obj2="merchandizing_avtr"></div><div ico-obj3="merchandizing_avtr"></div><div ico-obj4="merchandizing_avtr"></div></div>',

waiternwaitres_avtr:'<div ico="waiternwaitres_avtr"><div ico-obj1="waiternwaitres_avtr"></div><div ico-obj2="waiternwaitres_avtr"></div><div ico-obj3="waiternwaitres_avtr"></div><div ico-obj4="waiternwaitres_avtr"></div></div>',

greetersushers_avtr:'<div ico="greetersushers_avtr"><div ico-obj1="greetersushers_avtr"></div><div ico-obj2="greetersushers_avtr"></div><div ico-obj3="greetersushers_avtr"></div><div ico-obj4="greetersushers_avtr"></div></div>',

realestate_avtr:'<div ico="realestate_avtr"><div ico-obj1="realestate_avtr"></div><div ico-obj2="realestate_avtr"></div><div ico-obj3="realestate_avtr"></div></div>',

intradedev_avtr:'<div ico="intradedev_avtr"><div ico-obj1="intradedev_avtr"></div><div ico-obj2="intradedev_avtr"></div></div>',
//SERVICES/BUSINESS ENDS HERE

//ART AND ENTERTAINMENT CATEGORY BEGINS HERE
artdevofficer_avtr:'<div ico="artdevofficer_avtr"><div ico-obj1="artdevofficer_avtr"></div><div ico-obj2="artdevofficer_avtr"></div><div ico-obj3="artdevofficer_avtr"></div><div ico-obj4="artdevofficer_avtr"></div></div>',

fineart_avtr:'<div ico="fineart_avtr"><div ico-obj1="fineart_avtr"></div><div ico-obj2="fineart_avtr"></div></div>',

acting_avtr:'<div ico="acting_avtr"><div ico-obj1="acting_avtr"></div><div ico-obj2="acting_avtr"></div></div>',

textile_avtr:'<div ico="textile_avtr"><div ico-obj1="textile_avtr"></div><div ico-obj2="textile_avtr"></div></div>',

interiordesigndeco_avtr:'<div ico="interiordesigndeco_avtr"><div ico-obj1="interiordesigndeco_avtr"></div><div ico-obj2="interiordesigndeco_avtr"></div><div ico-obj3="interiordesigndeco_avtr"></div></div>',

writer_avtr:'<div ico="writer_avtr"><div ico-obj1="writer_avtr"></div><div ico-obj2="writer_avtr"></div><div ico-obj3="writer_avtr"></div><div ico-obj4="writer_avtr"></div></div>',

dancer_avtr:'<div ico="dancer_avtr"><div ico-obj1="dancer_avtr"></div><div ico-obj2="dancer_avtr"></div><div ico-obj3="dancer_avtr"></div><div ico-obj4="dancer_avtr"></div><div ico-obj5="dancer_avtr"></div></div>',

fashiondesigner_avtr:'<div ico="fashiondesigner_avtr"><div ico-obj1="fashiondesigner_avtr"></div><div ico-obj2="fashiondesigner_avtr"></div><div ico-obj3="fashiondesigner_avtr"></div><div ico-obj4="fashiondesigner_avtr"></div></div>',

apparelfashion_avtr:'<div ico="apparelfashion_avtr"><div ico-obj1="apparelfashion_avtr"></div><div ico-obj2="apparelfashion_avtr"></div><div ico-obj3="apparelfashion_avtr"></div></div>',

makeupartist_avtr:'<div ico="makeupartist_avtr"><div ico-obj1="makeupartist_avtr"></div><div ico-obj2="makeupartist_avtr"></div><div ico-obj3="makeupartist_avtr"></div><div ico-obj4="makeupartist_avtr"></div></div>',

musician_avtr:'<div ico="musician_avtr"><div ico-obj1="musician_avtr"></div><div ico-obj2="musician_avtr"></div><div ico-obj3="musician_avtr"></div><div ico-obj4="musician_avtr"></div><div ico-obj5="musician_avtr"></div></div>',

musictutor_avtr:'<div ico="musictutor_avtr"><div ico-obj1="musictutor_avtr"></div><div ico-obj2="musictutor_avtr"></div><div ico-obj3="musictutor_avtr"></div><div ico-obj4="musictutor_avtr"></div><div ico-obj5="musictutor_avtr"></div></div>',

performingart_avtr:'<div ico="performingart_avtr"><div ico-obj1="performingart_avtr"></div><div ico-obj2="performingart_avtr"></div><div ico-obj3="performingart_avtr"></div></div>',

setdesign_avtr:'<div ico="setdesign_avtr"><div ico-obj1="setdesign_avtr"></div><div ico-obj2="setdesign_avtr"></div><div ico-obj3="setdesign_avtr"></div></div>',

proofreader_avtr:'<div ico="proofreader_avtr"><div ico-obj1="proofreader_avtr"></div><div ico-obj2="proofreader_avtr"></div><div ico-obj3="proofreader_avtr"></div><div ico-obj4="proofreader_avtr"></div></div>',

critic_avtr:'<div ico="critic_avtr"><div ico-obj1="critic_avtr"></div><div ico-obj2="critic_avtr"></div><div ico-obj3="critic_avtr"></div><div ico-obj4="critic_avtr"></div><div ico-obj5="critic_avtr"></div></div>',

copywritter_avtr:'<div ico="copywritter_avtr"><div ico-obj1="copywritter_avtr"></div><div ico-obj2="copywritter_avtr"></div><div ico-obj3="copywritter_avtr"></div><div ico-obj4="copywritter_avtr"></div><div ico-obj5="copywritter_avtr"></div><div ico-obj6="copywritter_avtr"></div></div>',

copyeditor_avtr:'<div ico="copyeditor_avtr"><div ico-obj1="copyeditor_avtr"></div><div ico-obj2="copyeditor_avtr"></div><div ico-obj3="copyeditor_avtr"></div><div ico-obj4="copyeditor_avtr"></div><div ico-obj5="copyeditor_avtr"></div><div ico-obj6="copyeditor_avtr"></div></div>',

photocopy_avtr:'<div ico="photocopy_avtr"><div ico-obj1="photocopy_avtr"></div><div ico-obj2="photocopy_avtr"></div><div ico-obj3="photocopy_avtr"></div></div>',

radioproducer_avtr:'<div ico="radioproducer_avtr"><div ico-obj1="radioproducer_avtr"></div><div ico-obj2="radioproducer_avtr"></div><div ico-obj3="radioproducer_avtr"></div></div>',

literature_avtr:'<div ico="literature_avtr"><div ico-obj1="literature_avtr"></div><div ico-obj2="literature_avtr"></div></div>',

architecture_avtr:'<div ico="architecture_avtr"><div ico-obj1="architecture_avtr"></div><div ico-obj2="architecture_avtr"></div><div ico-obj3="architecture_avtr"></div></div>',

curator_avtr:'<div ico="curator_avtr"><div ico-obj1="curator_avtr"></div><div ico-obj2="curator_avtr"></div><div ico-obj3="curator_avtr"></div><div ico-obj4="curator_avtr"></div><div ico-obj5="curator_avtr"></div><div ico-obj6="curator_avtr"></div></div>',

museumgallery_avtr:'<div ico="museumgallery_avtr"><div ico-obj1="museumgallery_avtr"></div><div ico-obj2="museumgallery_avtr"></div><div ico-obj3="museumgallery_avtr"></div></div>',

model_avtr:'<div ico="model_avtr"><div ico-obj1="model_avtr"></div><div ico-obj2="model_avtr"></div><div ico-obj3="model_avtr"></div><div ico-obj4="model_avtr"></div><div ico-obj5="model_avtr"></div></div>',
//ART AND ENTERTAINMENT CATEGORY ENDS HERE
	};