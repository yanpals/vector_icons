// smiley

var emoticon = {
	smile: {
		html: '<div ico="smile"><div ico-obj="smile"></div></div>',
		codes: {
			0:':)',
			1:':]',
			2:'=)',
			3:':-)'
			}
		},
	sad: {
		html: '<span ico="smiley"><div ico="sad"><div ico-obj1="sad"></div></div></span>',
		codes: {
			0:':(',
			1:':[',
			2:':-(',
			3:'=['
			}
		},
	laugh: {
		html: '<div ico="laugh"><div ico-obj1="laugh"></div></div>',
		codes: {
			0:'=D'
			}
		},
	straight_face: {
		html: '<div ico="straight_face"><div ico-obj="straight_face"></div></div>',
		codes: {
			0:':|'
			}
		},
	lovestruck: {
		html: '<div ico="lovestruck"><div ico-obj1="lovestruck"></div><div ico-obj2="lovestruck"></div><div ico-obj3="lovestruck"></div></div>',
		codes: {
			0:'<3<3'
			}
		},
	tongue: {
		html: '<div ico="tongue"><div ico-obj1="tongue"></div><div ico-obj2="tongue"></div></div>',
		codes: {
			0:':P',
			1:'=P',
			2:':-P',
			3:'(:-p)',
			4:'>:P',
			5:':-r',
			6:':pp',
			7:'þ',
			8:':-)~',
			9:':(,)'
			}
		},
	shocked: {
		html: '<div ico="shocked"><div ico-obj1="shocked"></div><div ico-obj2="shocked"></div></div>',
		codes: {
			0:':O',
			1:':-o',
			2:'o_o',
			3:'*SHOCKED*'
			}
		},
	wink: {
		html: '<div ico="wink"><div ico-obj="wink"></div></div>',
		codes: {
			0:';)',
			1:';-)',
			2:';)',
			3:'~_^',
			4:'(-!',
			5:';()',
			6:'!-)'
			}
		},
	cool: {
		html: '<div ico="cool"><div ico-obj="cool"></div></div>',
		codes: {
			0:'B'
			}
		},
	upset: {
		html: '<div ico="upset"><div ico-obj1="upset"></div><div ico-obj2="upset"></div></div>',
		codes: {
			0:'>:(',
			1:'>.<',
			2:'--"',
			3:'}:(',
			4:'D:<',
			5:'>=P',
			6:'>:*'
			}
		},
	nerd: {
		html: '<div ico="nerd"><div ico-obj1="nerd"></div><div ico-obj2="nerd"></div></div>',
		codes: {
			0:'B|',
			1:':-B',
			2:'8-|',
			3:'8-B',
			4:'*8-I',
			5:':-=H',
			6:'|-(:-)'
			}
		},
	heart: {
		html: '<div ico="heart"></div>',
		codes: {
			0:'<3',
			1:'?',
			2:'S2',
			3:'<u3'
			}
		},
	bored: {
		html: '<div ico="bored"><div ico-obj="bored"></div></div>',
		codes: {
			0:'-_-',
			1:':-o',
			2:'(-_-)',
			3:'@-@',
			4:':/'
			}
		},
	yawn: {
		html: '<div ico="yawn"><div ico-obj1="yawn"></div><div ico-obj2="yawn"></div></div>',
		codes: {
			0:'(yawn)',
			1:'|-O',
			2:'(:|'
			}
		},
	pacman: {
		html: '<div ico="pacman"></div>',
		codes: {
			0:':v',
			1:'(<. . . .',
			2:'',
			3:'(:V)',
			4:':v .....'
			}
		},
	unsure: {
		html: '<div ico="unsure"><div ico-obj1="unsure"></div><div ico-obj2="unsure"></div></div>',
		codes: {
			0:':/',
			1:'>->'
			}
		},	
	crying: {
		html: '<div ico="crying"><div ico-obj1="crying"></div><div ico-obj2="crying"></div><div ico-obj3="crying"></div></div>',
		codes: {
			0:':(',
			1:'|-O',
			2:'(:|'
			}
		},
	devil: {
		html: '<div ico="devil"><div ico-obj1="devil"></div><div ico-obj2="devil"></div><div ico-obj3="devil"></div><div ico-obj4="devil"></div></div>',
		codes: {
			0:'3:)',
			1:'>:)',
			2:',}:-)',
			3:'(6)',
			4:']:->',
			5:'()}:o)',
			6:'}=^{|~'
			}
		},
	angel: {
		html: '<div ico="angel"><div ico-obj1="angel"></div><div ico-obj2="angel"></div></div>',
		codes: {
			0:'O:)',
			1:'O:-)',
			2:'0=)',
			3:'0-)<',
			4:'^j^',
			5:'(a)',
			6:'0;)',
			7:'O-)'
			}
		},
	kiss: {
		html: '<div ico="kiss"><div ico-obj1="kiss"></div><div ico-obj2="kiss"></div><div ico-obj3="kiss"></div></div>',
		codes: {
			0:':*',
			1:':-)*',
			2:':-*',
			3:'(:-p)',
			4:':-X',
			5:'= #',
			6:';-(!)',
			7:':*)',
			8:'8^*',
			9:':+',
			10:':P'
			}
		},
	confused: {
		html: '<div ico="confused"><div ico-obj1="confused"></div><div ico-obj2="confused"></div></div>',
		codes: {
			0:':O',
			1:':-o',
			2:'o_o',
			3:'*SHOCKED*'
			}
		},
	embarassed: {
		html: '<div ico="embarassed"><div ico-obj1="embarassed"></div></div>',
		codes: {
			0:':$'
			}
		},
	blush: {
		html: '<div ico="blush"><div ico-obj1="blush"></div><div ico-obj2="blush"></div></div>',
		codes: {
			0:':-*>',
			1:':">',
			2:'=">',
			3:':-">',
			4:'=^}',
			5:'>//<',
			6:':,)'
			}
		},
	worried: {
		html: '<div ico="worried"><div ico-obj1="worried"></div><div ico-obj2="worried"></div></div>',
		codes: {
			0:':-S',
			1:'>w<',
			2:'<00>~ '
			}
		},
	wondering: {
		html: '<div ico="wondering"><div ico-obj1="wondering"></div><div ico-obj2="wondering"></div><div ico-obj3="wondering"></div></div>',
		codes: {
			0:'o.o?',
			1:':^)'
			}
		},
	thinking: {
		html: '<div ico="thinking"><div ico-obj1="thinking"></div><div ico-obj2="thinking"></div></div>',
		codes: {
			0:':-?',
			1:'*-)',
			2:':?'
			}
		},
	whew: {
		html: '<div ico="whew"><div ico-obj1="whew"></div><div ico-obj2="whew"></div></div>',
		codes: {
			0:'#:-s'
			}
		},
	sick: {
		html: '<div ico="sick"><div ico-obj1="sick"></div><div ico-obj2="sick"></div></div>',
		codes: {
			0:'-&'
			}
		},
	waiting: {
		html: '<div ico="waiting"><div ico-obj1="waiting"></div><div ico-obj2="waiting"></div><div ico-obj3="waiting"></div></div>',
		codes: {
			0:':-w'
			}
		},
	sweat: {
		html: '<div ico="sweat"><div ico-obj1="sweat"></div><div ico-obj2="sweat"></div><div ico-obj3="sweat"></div></div>',
		codes: {
			0:'>:(',
			1:'>.<',
			2:'--"',
			3:'}:(',
			4:'D:<',
			5:'>=P',
			6:'>:*'
			}
		},
	speechless: {
		html: '<div ico="speechless"><div ico-obj1="speechless"></div><div ico-obj2="speechless"></div><div ico-obj3="speechless"></div></div>',
		codes: {
			0:':|',
			1:'=#'
			}
		},
	snooze: {
		html: '<div ico="snooze"><div ico-obj1="snooze"></div><div ico-obj2="snooze"></div><div ico-obj3="snooze"></div><div ico-obj4="snooze"></div></div>',
		codes: {
			0:'|-)',
			1:'I=)'
			}
		},
	poke: {
		html: '<div ico="poke"><div ico-obj1="poke"></div><div ico-obj2="poke"></div></div>',
		codes: {
			0:':&',
			1:':-&',
			}
		},
	lipsealed: {
		html: '<div ico="lipsealed"><div ico-obj1="lipsealed"></div><div ico-obj2="lipsealed"></div></div>',
		codes: {
			0:':x',
			1:':#',
			2:':=X'
			}
		},
	not_listening: {
		html: '<div ico="not_listening"><div ico-obj1="not_listening"></div><div ico-obj2="not_listening"></div><div ico-obj3="not_listening"></div><div ico-obj4="not_listening"></div></div>',
		codes: {
			0:'(lalala)'
			}
		},
	grin: {
		html: '<div ico="grin"><div ico-obj1="grin"></div><div ico-obj2="grin"></div></div>',
		codes: {
			0:':D',
			1:':->',
			2:'¦-D'
			}
		},
	rofl: {
		html: '<div ico="rofl"><div ico-obj1="rofl"></div><div ico-obj2="rofl"></div></div>',
		codes: {
			0:'*rofl*'
			}
		},
	};