// Vectors

	
var loader = {
	loader60: '<div vtrs1="loader_ring"> <div vtrs1-obj1="loader_ring_light"></div><div vtrs1-obj2="loader_ring_track"></div><div vtrs1-obj3="loader_bg"></div></div>',	
	loader120: '<div vtrs2="loader_ring"> <div vtrs2-obj1="loader_ring_light"></div><div vtrs2-obj2="loader_ring_track"></div><div vtrs2-obj3="loader_bg"></div></div>',
	spinloader: '<div vtrs3="loader_ring"> <div vtrs3-obj1="loader_ring_light"></div><div vtrs3-obj2="loader_ring_track"></div><div vtrs3-obj3="loader_bg"></div></div>',
	whitespinloader: '<div whitevtrs3="loader_ring"> <div whitevtrs3-obj1="loader_ring_light"></div><div whitevtrs3-obj2="loader_ring_track"></div><div whitevtrs3-obj3="loader_bg"></div></div>',
	broadcastloader: '<div data-box="box"> <div data-box1="loading"></div></div>',
	orangebgbroadcastloader: '<div data-box="orangebox"> <div data-box1="orangeloading"></div></div>',
	progressloader: '<div id="wrapper"> <div class="hold"> <div class="pie na"></div> <div class="pie fill"></div> </div> <div class="hold gt2"> <div class="pie na"></div> </div> <div class="mask clear"></div> </div>', 
	orangeloader60: '<div data="loader_ring"> <div data-obj1="loader_ring_light"></div><div data-obj2="loader_ring_track"></div><div data-obj3="loader_bg"></div></div>',
	orangeloader120: '<div data-ring="loader_ring"> <div data-ring-obj1="loader_ring_light"></div><div data-ring-obj2="loader_ring_track"></div><div data-ring-obj3="loader_bg"></div></div>',
	rotateloader30: '<div class="showbox30"> <div class="loader30"> <svg class="circular30" viewBox="25 25 50 50"> <circle class="path30" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/> </svg> </div> </div>',
	rotateloader60: '<div class="showbox60"> <div class="loader60"> <svg class="circular60" viewBox="25 25 50 50"> <circle class="path60" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/> </svg> </div> </div>',
	rotateloader90: '<div class="showbox90"> <div class="loader90"> <svg class="circular90" viewBox="25 25 50 50"> <circle class="path90" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/> </svg> </div> </div>',
	rotateloader120: '<div class="showbox"> <div class="loader"> <svg class="circular" viewBox="25 25 50 50"> <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/> </svg> </div> </div>',
	commentloader: '<div class="page"><div class="loader"><span class="bubble bubble-one"></span><span class="bubble bubble-two"></span><span class="bubble bubble-three"></span></div></div>',
	commentloadersmall: '<div class="loader2"><span class="bubble2 bubble-one"></span><span class="bubble2 bubble-two"></span><span class="bubble2 bubble-three"></span></div>',
	logoloader: '<div class="logoloader"><div logobg="logo_bg"></div></div>',


};